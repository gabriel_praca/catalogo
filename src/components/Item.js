
import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';

export default class Item extends React.Component {
    render() {
        return (
            <View style={styles.item}>
                <View style={styles.imageContainer}>
                    <Image 
                        style={styles.itemImage} 
                        source={{ uri: this.props.item.foto }} 
                    />
                </View>
                <View style={styles.itemDetails}>
                    <Text style={styles.title}> {this.props.item.titulo} </Text>
                    <Text style={styles.value}> R$: {this.props.item.valor} </Text>
                    <Text style={styles.details}> Local: {this.props.item.local_anuncio} </Text>
                    <Text style={styles.details}> Dt. Publicação: {this.props.item.data_publicacao} </Text>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    item: {
        borderWidth: 0.5,
        borderColor: '#999',
        backgroundColor: 'white',
        margin: 10,
        padding: 10,
        flexDirection: 'row',
    },
    imageContainer: {
        width: 105, 
        height: 105
    },
        itemImage: {
            width: 100, 
            height: 100
        },
    itemDetails: {
        marginLeft: 20,
        flex: 1,
    },
        title: {
            fontSize: 18,
            color: 'blue',
            marginBottom: 5,
        },
        value: {
            fontSize: 16,
            fontWeight: 'bold',
        },
        details: {
            fontSize: 16,
        }
});
