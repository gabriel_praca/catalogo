
import axios from 'axios';

import React from 'react';
import { ScrollView } from 'react-native';

import Item from './Item.js';

export default class ItemList extends React.Component {
    //4-etapa final depois do didMount
    constructor(props) {
        super(props);

        this.state = { itemList: [] };
    }

    //1-antes de renderizar 
    componentWillMount() {
        axios.get('http://faus.com.br/recursos/c/dmairr/api/itens.html')
            .then(response => { this.setState({ itemList: response.data }); })
            .catch(() => { console.log('erro ao recuperar os dados'); });
    }

    //3-Depois de renderizar
    componentDidMount() {
        console.log('didMount');
    }

    //renderiza
    render() {
    return (
      <ScrollView style={{ backgroundColor: '#DDD' }}>
        { this.state.itemList.map(item => <Item key={item.titulo} item={item} />) }
      </ScrollView>
    );
  }
}
